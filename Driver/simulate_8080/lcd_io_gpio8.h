/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __LCD_IO_GPIO8_H
#define __LCD_IO_GPIO8_H

/* Includes -------------------------------------------------------------------*/
#include  "hal.h"
#include  "acm32f42x_coreboard.h"

#include "gpio_def.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/

/* Exported define ------------------------------------------------------------*/
/****************** LCD Control Pin Mapping *************************/
#define LCD_CS          B, 1
#define LCD_RS          B, 2
#define LCD_WR          B, 4
#define LCD_RD          B, 5
#define LCD_RST         B, 12 

/****************** LCD Data Pin Mapping *************************/
#define LCD_D0          A, 0
#define LCD_D1          A, 1
#define LCD_D2          A, 2
#define LCD_D3          A, 3
#define LCD_D4          A, 4
#define LCD_D5          A, 5
#define LCD_D6          A, 6
#define LCD_D7          A, 7

#define LCD_DAT_PORT    GPIOA
#define LCD_CMD_PORT    GPIOB

#define LCD_WRITE_DELAY 1
#define LCD_READ_DELAY  20

/*********************** 引脚操作 *************************/
//命令/数据引脚
#define LCD_RS_CMD          GPIOX_CLR(LCD_RS)
#define LCD_RS_DATA         GPIOX_SET(LCD_RS)

//复位引脚
#define LCD_RST_ON          GPIOX_CLR(LCD_RST)
#define LCD_RST_OFF         GPIOX_SET(LCD_RST)

//片选引脚
#define LCD_CS_ON           GPIOX_CLR(LCD_CS)
#define LCD_CS_OFF          GPIOX_SET(LCD_CS)

#define LCD_WR_H            GPIOX_SET(LCD_WR)
#define LCD_WR_L            GPIOX_CLR(LCD_WR)

#define LCD_RD_H            GPIOX_SET(LCD_RD)
#define LCD_RD_L            GPIOX_CLR(LCD_RD)

/* Exported macro -------------------------------------------------------------*/
#define IS_LCD_GPIO_PORT_NUM_SAME()                  \
    ((GPIOX_PORTNUM(LCD_D0) == GPIOX_PORTNUM(LCD_D1))\
  && (GPIOX_PORTNUM(LCD_D1) == GPIOX_PORTNUM(LCD_D2))\
  && (GPIOX_PORTNUM(LCD_D2) == GPIOX_PORTNUM(LCD_D3))\
  && (GPIOX_PORTNUM(LCD_D3) == GPIOX_PORTNUM(LCD_D4))\
  && (GPIOX_PORTNUM(LCD_D4) == GPIOX_PORTNUM(LCD_D5))\
  && (GPIOX_PORTNUM(LCD_D5) == GPIOX_PORTNUM(LCD_D6))\
  && (GPIOX_PORTNUM(LCD_D6) == GPIOX_PORTNUM(LCD_D7)))

#define IS_LCD_GPIO_PIN_NUM_IN_ORDER()           \
    ((GPIOX_PIN(LCD_D0) + 1 == GPIOX_PIN(LCD_D1))\
  && (GPIOX_PIN(LCD_D1) + 1 == GPIOX_PIN(LCD_D2))\
  && (GPIOX_PIN(LCD_D2) + 1 == GPIOX_PIN(LCD_D3))\
  && (GPIOX_PIN(LCD_D3) + 1 == GPIOX_PIN(LCD_D4))\
  && (GPIOX_PIN(LCD_D4) + 1 == GPIOX_PIN(LCD_D5))\
  && (GPIOX_PIN(LCD_D5) + 1 == GPIOX_PIN(LCD_D6))\
  && (GPIOX_PIN(LCD_D6) + 1 == GPIOX_PIN(LCD_D7)))
  
/* Exported variables ---------------------------------------------------------*/

/* Exported function prototypes -----------------------------------------------*/
//Init
void LCD_IO_Init(void);

//Write Command and Data Base Func
void LCD_IO_WriteCmd8(uint8_t Cmd);
void LCD_IO_WriteCmd16(uint16_t Cmd);
void LCD_IO_WriteData8(uint8_t Data);
void LCD_IO_WriteData16(uint16_t Data);

//Write Command and Data  
void LCD_IO_WriteCmd8DataFill16(uint8_t Cmd, uint16_t Data, uint32_t Size);
void LCD_IO_WriteCmd8MultipleData8(uint8_t Cmd, uint8_t *pData, uint32_t Size);
void LCD_IO_WriteCmd8MultipleData16(uint8_t Cmd, uint16_t *pData, uint32_t Size);
void LCD_IO_WriteCmd16DataFill16(uint16_t Cmd, uint16_t Data, uint32_t Size);
void LCD_IO_WriteCmd16MultipleData8(uint16_t Cmd, uint8_t *pData, uint32_t Size);
void LCD_IO_WriteCmd16MultipleData16(uint16_t Cmd, uint16_t *pData, uint32_t Size);

// Read Data
uint8_t LCD_IO_ReadData8();
void LCD_IO_ReadCmd8MultipleData8(uint8_t Cmd, uint8_t *pData, uint32_t Size, uint32_t DummySize);
void LCD_IO_ReadCmd8MultipleData16(uint8_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize);
void LCD_IO_ReadCmd8MultipleData24to16(uint8_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize);
void LCD_IO_ReadCmd16MultipleData8(uint16_t Cmd, uint8_t *pData, uint32_t Size, uint32_t DummySize);
void LCD_IO_ReadCmd16MultipleData16(uint16_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize);
void LCD_IO_ReadCmd16MultipleData24to16(uint16_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize);

#ifdef __cplusplus
}
#endif

#endif  /*__LCD_IO_GPIO8_H */
/******************************************* end of file **************************************************/
