/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Includes ------------------------------------------------------------------*/
#include "lcd_io_gpio8.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/


/* 判断8个数据管脚是否为同一端口，并且引脚号递增 */
#if IS_LCD_GPIO_PORT_NUM_SAME()
#if IS_LCD_GPIO_PIN_NUM_IN_ORDER()
/* LCD data pins on n..n+7 pin (ex. B6,B7,B8,B9,B10,B11,B12,B13) */
#define LCD_AUTOOPT
#endif /* D0..D7 pin order */
#endif /* D0..D7 port same */

//-----------------------------------------------------------------------------
/* data pins set to output direction */
#ifndef LCD_DIRWRITE
#ifdef  LCD_AUTOOPT
#define LCD_DIRWRITE  GPIOX_PORT(LCD_D0)->MD = (GPIOX_PORT(LCD_D0)->MD & ~(0xFFFF << (2 * GPIOX_PIN(LCD_D0)))) | (0x5555 << (2 * GPIOX_PIN(LCD_D0)));
#else
#define LCD_DIRWRITE { \
  GPIOX_MD(MODE_OUT, LCD_D0); GPIOX_MD(MODE_OUT, LCD_D1);\
  GPIOX_MD(MODE_OUT, LCD_D2); GPIOX_MD(MODE_OUT, LCD_D3);\
  GPIOX_MD(MODE_OUT, LCD_D4); GPIOX_MD(MODE_OUT, LCD_D5);\
  GPIOX_MD(MODE_OUT, LCD_D6); GPIOX_MD(MODE_OUT, LCD_D7);}
#endif
#endif

//-----------------------------------------------------------------------------
/* data pins set to input direction */
#ifndef LCD_DIRREAD
#ifdef  LCD_AUTOOPT
#define LCD_DIRREAD  GPIOX_PORT(LCD_D0)->MD = (GPIOX_PORT(LCD_D0)->MD & ~(0xFFFF << (2 * GPIOX_PIN(LCD_D0)))) | (0x0000 << (2 * GPIOX_PIN(LCD_D0)));
#else
#define LCD_DIRREAD { \
  GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D0); GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D1);\
  GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D2); GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D3);\
  GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D4); GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D5);\
  GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D6); GPIOX_MD(MODE_DIGITAL_INPUT, LCD_D7);}
#endif
#endif

//-----------------------------------------------------------------------------
/* 8 bit data write to the data pins */
#ifndef LCD_WRITE
#ifdef  LCD_AUTOOPT
#if GPIOX_PIN(LCD_D0) == 0
#define LCD_WRITE(dt)   GPIOX_PORT(LCD_D0)->BSC = (dt) | (0xFF << 16)
#else
#define LCD_WRITE(dt) { \
  GPIOX_PORT(LCD_D0)->BSC = (dt << GPIOX_PIN(LCD_D0)) | (0xFF << (GPIOX_PIN(LCD_D0) + 16));}
#endif
#else
#define LCD_WRITE(dt) {;                                   \
  if(dt & 0x01) GPIOX_SET(LCD_D0); else GPIOX_CLR(LCD_D0); \
  if(dt & 0x02) GPIOX_SET(LCD_D1); else GPIOX_CLR(LCD_D1); \
  if(dt & 0x04) GPIOX_SET(LCD_D2); else GPIOX_CLR(LCD_D2); \
  if(dt & 0x08) GPIOX_SET(LCD_D3); else GPIOX_CLR(LCD_D3); \
  if(dt & 0x10) GPIOX_SET(LCD_D4); else GPIOX_CLR(LCD_D4); \
  if(dt & 0x20) GPIOX_SET(LCD_D5); else GPIOX_CLR(LCD_D5); \
  if(dt & 0x40) GPIOX_SET(LCD_D6); else GPIOX_CLR(LCD_D6); \
  if(dt & 0x80) GPIOX_SET(LCD_D7); else GPIOX_CLR(LCD_D7); }
#endif
#endif

//-----------------------------------------------------------------------------
/* 8 bit data read from the data pins */
#ifndef LCD_READ
#ifdef  LCD_AUTOOPT
#define LCD_READ(dt) {                          \
  dt = GPIOX_PORT(LCD_D0)->IDATA >> GPIOX_PIN(LCD_D0); }
#else
#define LCD_READ(dt) {                       \
  if(GPIOX_IDATA(LCD_D0)) dt = 1; else dt = 0; \
  if(GPIOX_IDATA(LCD_D1)) dt |= 0x02;          \
  if(GPIOX_IDATA(LCD_D2)) dt |= 0x04;          \
  if(GPIOX_IDATA(LCD_D3)) dt |= 0x08;          \
  if(GPIOX_IDATA(LCD_D4)) dt |= 0x10;          \
  if(GPIOX_IDATA(LCD_D5)) dt |= 0x20;          \
  if(GPIOX_IDATA(LCD_D6)) dt |= 0x40;          \
  if(GPIOX_IDATA(LCD_D7)) dt |= 0x80;          }
#endif
#endif

/* 读/写延时 */
#if     LCD_WRITE_DELAY == 0
#define LCD_WR_DELAY
#elif   LCD_WRITE_DELAY == 1
#define LCD_WR_DELAY          GPIOX_CLR(LCD_WR)
#else
#define LCD_WR_DELAY          LCD_IO_Delay(LCD_WRITE_DELAY - 2)
#endif

#if     LCD_READ_DELAY == 0
#define LCD_RD_DELAY
#elif   LCD_READ_DELAY == 1
#define LCD_RD_DELAY          GPIOX_CLR(LCD_RD)
#else
#define LCD_RD_DELAY          LCD_IO_Delay(LCD_READ_DELAY - 2)
#endif

/* 读/写操作（8bit） */
#define LCD_DUMMY_READ        { GPIOX_CLR(LCD_RD); LCD_RD_DELAY; GPIOX_SET(LCD_RD); }
#define LCD_DATA8_WRITE(dt)   { lcd_data8 = dt; LCD_WRITE(lcd_data8); GPIOX_CLR(LCD_WR); LCD_WR_DELAY; GPIOX_SET(LCD_WR); }
#define LCD_DATA8_READ(dt)    { GPIOX_CLR(LCD_RD); LCD_RD_DELAY; LCD_READ(dt); GPIOX_SET(LCD_RD); }
#define LCD_CMD8_WRITE(cmd)   { LCD_RS_CMD; LCD_DATA8_WRITE(cmd); LCD_RS_DATA; }

/* 读/写操作（16bit） */
#if LCD_REVERSE16 == 0
#define LCD_CMD16_WRITE(cmd16)  {LCD_RS_CMD; LCD_DATA8_WRITE(cmd16 >> 8); LCD_DATA8_WRITE(cmd16); LCD_RS_DATA; }
#define LCD_DATA16_WRITE(d16)   {LCD_DATA8_WRITE(d16 >> 8); LCD_DATA8_WRITE(d16); }
#define LCD_DATA16_READ(dh, dl) {LCD_DATA8_READ(dh); LCD_DATA8_READ(dl); }
#else
#define LCD_CMD16_WRITE(cmd)    {LCD_RS_CMD; LCD_DATA8_WRITE(cmd); LCD_DATA8_WRITE(cmd >> 8); LCD_RS_DATA; }
#define LCD_DATA16_WRITE(data)  {LCD_DATA8_WRITE(data); LCD_DATA8_WRITE(data >> 8); }
#define LCD_DATA16_READ(dh, dl) {LCD_DATA8_READ(dl); LCD_DATA8_READ(dh); }
#endif

/* Private variables ---------------------------------------------------------*/
/* 8 bit temp data */
uint8_t  lcd_data8;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void LCD_IO_Delay(volatile uint32_t c)
{
    while(c--);
}

void LCD_Delay(uint32_t Delay)
{
    HAL_Delay(Delay);
}

void LCD_IO_Init(void)
{ 
	GPIO_InitTypeDef    GPIO_8080 = {0};
    
    __HAL_RCC_GPIOA_CLK_ENABLE();
    __HAL_RCC_GPIOB_CLK_ENABLE();
       
	//8080 D0-D17
	GPIO_8080.Pin       = GPIOX_PINx(LCD_D0) | GPIOX_PINx(LCD_D1) | GPIOX_PINx(LCD_D2) | GPIOX_PINx(LCD_D3) | 
                          GPIOX_PINx(LCD_D4) | GPIOX_PINx(LCD_D5) | GPIOX_PINx(LCD_D6) | GPIOX_PINx(LCD_D7);
	GPIO_8080.Mode      = GPIO_MODE_OUTPUT_PP;
	GPIO_8080.Pull      = GPIO_PULLUP;
    GPIO_8080.Drive     = GPIO_DRIVE_LEVEL3;
	GPIO_8080.Alternate = GPIO_FUNCTION_0;

	HAL_GPIO_Init(LCD_DAT_PORT, &GPIO_8080);

	//WR,RD.CS,DC,RST
	GPIO_8080.Pin       = (GPIOX_PINx(LCD_CS) | GPIOX_PINx(LCD_RS) | GPIOX_PINx(LCD_WR) | GPIOX_PINx(LCD_RD) | GPIOX_PINx(LCD_RST));
	GPIO_8080.Mode      = GPIO_MODE_OUTPUT_PP;
	GPIO_8080.Pull      = GPIO_PULLUP;
    GPIO_8080.Drive     = GPIO_DRIVE_LEVEL3;
	GPIO_8080.Alternate = GPIO_FUNCTION_0;
	HAL_GPIO_Init(LCD_CMD_PORT, &GPIO_8080);
	
    LCD_RST_OFF;
    LCD_RST_OFF;
    LCD_CS_OFF;
    LCD_WR_H;
    LCD_RD_H;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd8(uint8_t Cmd)
{
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd16(uint16_t Cmd)
{
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteData8(uint8_t Data)
{
  LCD_CS_ON;
  LCD_DATA8_WRITE(Data);
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteData16(uint16_t Data)
{
  LCD_CS_ON;
  LCD_DATA16_WRITE(Data);
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd8DataFill16(uint8_t Cmd, uint16_t Data, uint32_t Size)
{
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);
  while(Size--)
  {
    LCD_DATA16_WRITE(Data);
  }
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd8MultipleData8(uint8_t Cmd, uint8_t *pData, uint32_t Size)
{
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);

  while(Size--)
  {
    LCD_DATA8_WRITE(*pData);
    pData ++;
  }
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd8MultipleData16(uint8_t Cmd, uint16_t *pData, uint32_t Size)
{
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);
  while(Size--)
  {
    LCD_DATA16_WRITE(*pData);
    pData ++;
  }
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd16DataFill16(uint16_t Cmd, uint16_t Data, uint32_t Size)
{
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  while(Size--)
  {
    LCD_DATA16_WRITE(Data);
  }
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd16MultipleData8(uint16_t Cmd, uint8_t *pData, uint32_t Size)
{
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  while(Size--)
  {
    LCD_DATA8_WRITE(*pData);
    pData ++;
  }
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
void LCD_IO_WriteCmd16MultipleData16(uint16_t Cmd, uint16_t *pData, uint32_t Size)
{
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  while(Size--)
  {
    LCD_DATA16_WRITE(*pData);
    pData ++;
  }
  LCD_CS_OFF;
}

//-----------------------------------------------------------------------------
uint8_t LCD_IO_ReadData8()
{
    uint8_t dat;
    LCD_CS_ON;
    LCD_DIRREAD;
    LCD_DATA8_READ(dat);
    LCD_CS_OFF;
    LCD_DIRWRITE;
    return dat;
}

//-----------------------------------------------------------------------------
void LCD_IO_ReadCmd8MultipleData8(uint8_t Cmd, uint8_t *pData, uint32_t Size, uint32_t DummySize)
{
  uint8_t  d;
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);
  LCD_DIRREAD;
  while(DummySize--)
    LCD_DUMMY_READ;
  while(Size--)
  {
    LCD_DATA8_READ(d);
    *pData = d;
    pData++;
  }
  LCD_CS_OFF;
  LCD_DIRWRITE;
}

//-----------------------------------------------------------------------------
void LCD_IO_ReadCmd8MultipleData16(uint8_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize)
{
  uint8_t  dl, dh;
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);
  LCD_DIRREAD;
  while(DummySize--)
    LCD_DUMMY_READ;

  while(Size--)
  {
    LCD_DATA16_READ(dh, dl);
    *pData = (dh << 8) | dl;
    pData++;
  }
  LCD_CS_OFF;
  LCD_DIRWRITE;
}

//-----------------------------------------------------------------------------
void LCD_IO_ReadCmd8MultipleData24to16(uint8_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize)
{
  uint8_t  rgb888[3];
  LCD_CS_ON;
  LCD_CMD8_WRITE(Cmd);
  LCD_DIRREAD;
  while(DummySize--)
    LCD_DUMMY_READ;
  while(Size--)
  {
    LCD_DATA8_READ(rgb888[0]);
    LCD_DATA8_READ(rgb888[1]);
    LCD_DATA8_READ(rgb888[2]);
    #if LCD_REVERSE16 == 0
    *pData = ((rgb888[0] & 0xF8) << 8 | (rgb888[1] & 0xFC) << 3 | rgb888[2] >> 3);
    #else
    *pData = __REVSH((rgb888[0] & 0xF8) << 8 | (rgb888[1] & 0xFC) << 3 | rgb888[2] >> 3);
    #endif
    pData++;
  }
  LCD_CS_OFF;
  LCD_DIRWRITE;
}

//-----------------------------------------------------------------------------
void LCD_IO_ReadCmd16MultipleData8(uint16_t Cmd, uint8_t *pData, uint32_t Size, uint32_t DummySize)
{
  uint8_t  d;
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  LCD_DIRREAD;
  while(DummySize--)
    LCD_DUMMY_READ;
  while(Size--)
  {
    LCD_DATA8_READ(d);
    *pData = d;
    pData++;
  }
  LCD_CS_OFF;
  LCD_DIRWRITE;
}

//-----------------------------------------------------------------------------
void LCD_IO_ReadCmd16MultipleData16(uint16_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize)
{
  uint8_t  dl, dh;
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  LCD_DIRREAD;
  while(DummySize--)
    LCD_DUMMY_READ;
  while(Size--)
  {
    LCD_DATA16_READ(dh, dl);
    *pData = (dh << 8) | dl;
    pData++;
  }
  LCD_CS_OFF;
  LCD_DIRWRITE;
}

//-----------------------------------------------------------------------------
void LCD_IO_ReadCmd16MultipleData24to16(uint16_t Cmd, uint16_t *pData, uint32_t Size, uint32_t DummySize)
{
  uint8_t  rgb888[3];
  LCD_CS_ON;
  LCD_CMD16_WRITE(Cmd);
  LCD_DIRREAD;
  while(DummySize--)
    LCD_DUMMY_READ;
  while(Size--)
  {
    LCD_DATA8_READ(rgb888[0]);
    LCD_DATA8_READ(rgb888[1]);
    LCD_DATA8_READ(rgb888[2]);
    #if LCD_REVERSE16 == 0
    *pData = ((rgb888[0] & 0xF8) << 8 | (rgb888[1] & 0xFC) << 3 | rgb888[2] >> 3);
    #else
    *pData = __REVSH((rgb888[0] & 0xF8) << 8 | (rgb888[1] & 0xFC) << 3 | rgb888[2] >> 3);
    #endif
    pData++;
  }
  LCD_CS_OFF;
  LCD_DIRWRITE;
}

/******************************************* end of file **************************************************/
