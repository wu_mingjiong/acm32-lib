/*
 * @Author: Wu
 * @Date: 2023/08/08
 * @LastEditTime: 2023/08/08
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __BSP_UART_H
#define __BSP_UART_H

#if defined (__CC_ARM) // If use AC5 Compiler
#pragma anon_unions
#endif

#ifdef __cplusplus
extern "C" {
#endif

/* Includes -------------------------------------------------------------------*/
#include "ACM32Fxx_HAL.h"

/* Exported macro -------------------------------------------------------------*/
#define RX_BUF_MAX_SIZE (1024U)

/* Exported typedef -----------------------------------------------------------*/
typedef struct 
{
	uint8_t DATA_RX_BUF[RX_BUF_MAX_SIZE];
	
	union
	{
	  __IO uint16_t InfAll;
		struct
		{
		  __IO uint16_t FramLength     :15;	//Serial receiving length
			__IO uint8_t FramFinishFlag;  //Serial port receiving complete sign
		}InfBit;
	};
}Struct_Usart_Fram, *pStruct_Usart_Fram; // Serial frame Buffer Struct

/* Exported define ------------------------------------------------------------*/
#define ENABLE_T35_TIMER_MODE 1

/** @brief Check whether the specified UART FLAG is set or not
  * @param __HANDLE__ specifies the flag UART handle
  * @param __FALG__　specifies the flag to check
  *　　　　This parameter can be one of the following args:
  *        @args @ref UART_MIS_OEI  BIT10
  *        @args @ref UART_MIS_BEI  BIT9
  *        @args @ref UART_MIS_PEI  BIT8
  *        @args @ref UART_MIS_FEI  BIT7
  *        @args @ref UART_MIS_RTI  BIT6
  *        @args @ref UART_MIS_TXI  BIT5
  *        @args @ref UART_MIS_RXI  BIT4
  * @retval The new state of __FLAG__ (TRUE or FALSE) 
  */
#define __HAL_UART_GET_FLAG(__HANDLE__, __FLAG__)     (((__HANDLE__)->Instance->MIS & (__FLAG__)) == (__FLAG__))

/** @brief Check whether the specified UART ERROR FLAG is set or not
  * @param __HANDLE__ specifies the flag UART handle
  * @param __FALG__　specifies the flag to check
  *　　　　This parameter can be one of the following args:
  *        @args @ref UART_ICR_OEI  BIT10
  *        @args @ref UART_ICR_BEI  BIT9
  *        @args @ref UART_ICR_PEI  BIT8
  *        @args @ref UART_ICR_FEI  BIT7
  *        @args @ref UART_ICR_RTI  BIT6
  *        @args @ref UART_ICR_TXI  BIT5
  *        @args @ref UART_ICR_RXI  BIT4
  * @retval The new state of __FLAG__ (TRUE or FALSE) 
  */
#define __HAL_UART_GET_ERR_FLAG(__HANDLE__, __FLAG__) (((__HANDLE__)->Instance->RIS & (__FLAG__)) == (__FLAG__))

/* Exported variables ---------------------------------------------------------*/
extern Struct_Usart_Fram Uart_Fram_Struct;
extern UART_HandleTypeDef UART_Handle;
extern UART_HandleTypeDef UART1_Handle;
extern uint8_t rx_buf;
/* Exported function prototypes -----------------------------------------------*/
//get receiving completion flag bit
__STATIC_INLINE uint8_t get_uart_recv_flag()
{
	return Uart_Fram_Struct.InfBit.FramFinishFlag;
}

//clear receiving completion flag bit
__STATIC_INLINE void clear_uart_recv_flag()
{
	Uart_Fram_Struct.InfBit.FramFinishFlag = 0;
}

#ifdef ENABLE_T35_TIMER_MODE
#define T35_TIM_CLOCK_FREQ    (10000)  
#define T35_TIMER_HANDLE      TIM3
#define T35_TIM_IRQHandler    TIM3_IRQHandler   

void BSP_T35_Timer_Config(uint32_t ulBaudRate);
#endif

void BSP_UART_Init(void);
HAL_StatusTypeDef BSP_UART_Receive_Byte_IT(UART_HandleTypeDef *huart, uint8_t *fu8_Data);

#ifdef __cplusplus
}
#endif

#endif  /*__BSP_UART_H */
/******************************************* end of file **************************************************/
