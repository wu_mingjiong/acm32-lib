/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Includes ------------------------------------------------------------------*/
#include "bsp_uart.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/
#define TIM_CR1_CEN  BIT0

#define __HAL_TIM_SET_COUNTER(__HANDLE__, __COUNTER__)  ((__HANDLE__)->Instance->CNT = (__COUNTER__))
#define __HAL_TIM_GET_COUNTER(__HANDLE__)               ((__HANDLE__)->Instance->CNT)

#define __HAL_TIM_ENABLE(__HANDLE__)                    ((__HANDLE__)->Instance->CR1 |= TIM_CR1_CEN)

#define __HAL_IS_TIM_ENABLE(__HANDLE__)                 READ_BIT((__HANDLE__)->Instance->CR1, TIM_CR1_CEN)
/* Private macro -------------------------------------------------------------*/
#define UART_BAUD_RATE 115200

/* Private variables ---------------------------------------------------------*/
UART_HandleTypeDef UART_Handle;
Struct_Usart_Fram Uart_Fram_Struct;
uint8_t rx_buf;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
void BSP_UART_ErrorCallback(UART_HandleTypeDef *huart);
void BSP_UART_RxCpltCallback(UART_HandleTypeDef *huart);
/* Application Layer ---------------------------------------------------------*/

/* Hardware Abstract Layer ---------------------------------------------------*/

/* Hardware Layer ------------------------------------------------------------*/

#if ENABLE_T35_TIMER_MODE
TIM_HandleTypeDef T35_TIM_Handler;
static uint32_t usTimerT35_50us;

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    xPortTimersInit
  * @brief   T35 Time Base Timer Init
  * @param   T35_50us:
  * @retval
  * @author  Wumingjiong 
  * @Data    2023-08-16
 **/
/* -------------------------------- end -------------------------------- */
static void xPortTimersInit(uint32_t T35_50us)
{
	/* Initialize a timer with an interrupt period of 50us here*/
    uint32_t timer_clock; 
    
    timer_clock = System_Get_APBClock(); 
    
    if (System_Get_SystemClock() != System_Get_APBClock())  // if hclk/pclk != 1, then timer clk = pclk * 2  
    {
       timer_clock =  System_Get_APBClock() << 1;    
    }
    
		T35_TIM_Handler.Instance = T35_TIMER_HANDLE;
		T35_TIM_Handler.Init.ARRPreLoadEn = TIM_ARR_PRELOAD_DISABLE;//TIM_ARR_PRELOAD_ENABLE;    
		T35_TIM_Handler.Init.ClockDivision = TIM_CLOCKDIVISION_DIV1; 
		T35_TIM_Handler.Init.CounterMode = TIM_COUNTERMODE_UP; 
		T35_TIM_Handler.Init.RepetitionCounter = 0;  
		T35_TIM_Handler.Init.Prescaler = (timer_clock  / T35_TIM_CLOCK_FREQ) - 1;  
		T35_TIM_Handler.Init.Period = 10 * 2 - 1; 
		
		HAL_TIMER_MSP_Init(&T35_TIM_Handler);   //interrrupt init       
		HAL_TIMER_Base_Init(&T35_TIM_Handler);    
//		HAL_TIM_ENABLE_IT(&T35_TIM_Handler, TIMER_INT_EN_UPD);       
//		HAL_TIMER_Base_Start(T35_TIM_Handler.Instance);   
}


/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    BSP_T35_Timer_Config
  * @brief   Config 
  * @param   ulBaudRate: [输入/出] 
  * @retval
  * @author  Wumingjiong 
  * @Data    2023-08-16
 **/
/* -------------------------------- end -------------------------------- */
void BSP_T35_Timer_Config(uint32_t ulBaudRate)
{
  if( ulBaudRate > 19200 )
	{
			usTimerT35_50us = 40;       /* 2000us. */
	}
	else
	{
			/* The timer reload value for a character is given by:
			 *
			 * ChTimeValue = Ticks_per_1s / ( Baudrate / 9 )
			 *             = 9 * Ticks_per_1s / Baudrate
			 *             = 180000 / Baudrate
			 * The reload for t3.5 is 1.5 times this value and similary
			 * for t3.5.
			 */
			usTimerT35_50us = ( 7UL * 180000UL ) / ( 2UL * ulBaudRate );
	}
	xPortTimersInit(usTimerT35_50us);
}

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    bsp_uart_frame_RxCpltCallback
  * @brief   frame buffer receive completely
  * @param   pUART_FRAM_Struct: 
  * @retval  None
  * @author  Wumingjiong 
  * @Data    2023-08-09
 **/
/* -------------------------------- end -------------------------------- */
void BSP_Uart_Frame_RxCpltCallback(pStruct_Usart_Fram pUART_FRAM_Struct)
{
  pUART_FRAM_Struct->InfBit.FramFinishFlag = 1;
	pUART_FRAM_Struct->DATA_RX_BUF[pUART_FRAM_Struct->InfBit.FramLength] = '\0';
}

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    T35_TIM_IRQHandler
  * @brief   T35 TIMER IRQ Handler
  * @param   None
  * @retval  None
  * @author  Wumingjiong 
  * @Data    2023-08-16
 **/
/* -------------------------------- end -------------------------------- */
void T35_TIM_IRQHandler(void)
{
	T35_TIMER_HANDLE->DIER &= ~(TIM_IT_UPDATE);
	T35_TIMER_HANDLE->CR1 &= (~BIT0); 
  BSP_Uart_Frame_RxCpltCallback(&Uart_Fram_Struct);
}

#endif

/* UART Init */
void BSP_UART_Init()
{
	UART_Handle.Instance        = UART2;    
	UART_Handle.Init.BaudRate   = UART_BAUD_RATE; 
	UART_Handle.Init.WordLength = UART_WORDLENGTH_8B;
	UART_Handle.Init.StopBits   = UART_STOPBITS_1;
	UART_Handle.Init.Parity     = UART_PARITY_NONE;
	UART_Handle.Init.Mode       = UART_MODE_TX_RX;
	UART_Handle.Init.HwFlowCtl  = UART_HWCONTROL_NONE;

	HAL_UART_Init(&UART_Handle);
	
	/* ENABLE FIFO */
	HAL_UART_Enable_Disable_FIFO(&UART_Handle,FUNC_DISABLE); 
	
#if ENABLE_T35_TIMER_MODE
	BSP_T35_Timer_Config(UART_BAUD_RATE);
#endif 
}

/**** !!!The following BSP FUNCTIONS don not use UART FIFO, which is different from HAL library function!!! ****/
static void BSP_UART_Fram_Receive_IT_Handler(UART_HandleTypeDef *huart, pStruct_Usart_Fram fram_struct, uint8_t *pData)
{
	if(fram_struct->InfBit.FramLength < RX_BUF_MAX_SIZE - 1)
	{
		fram_struct->DATA_RX_BUF[fram_struct->InfBit.FramLength++] = *pData;
	}
	else
	{ /* Overflow */
	  fram_struct->InfBit.FramLength = 0;
	}
	
	huart->Instance->ICR = UART_ICR_RXI;
	
	//BSP_UART_Receive_Byte_IT(&UART_Handle, &rx_buf);
}

HAL_StatusTypeDef BSP_UART_Receive_Byte_IT(UART_HandleTypeDef *huart, uint8_t *fu8_Data)
{
#if (USE_FULL_ASSERT == 1)
    if (!IS_UART_ALL_INSTANCE(huart->Instance))    return HAL_ERROR;
#endif
    
    if (huart->lu8_RxBusy == true) 
    {
        return HAL_BUSY;
    }
		
    huart->lu8_RxData   = fu8_Data;
    huart->lu8_RxBusy   = true; 
    huart->ErrorCode = HAL_UART_ERROR_NONE;  

    /* Clear RXI Status */
    huart->Instance->ICR = UART_ICR_RXI | UART_ICR_ERROR_BITS;
	
    /* Enable the UART Errors interrupt */
	  SET_BIT(huart->Instance->IE,UART_IE_OEI|UART_IE_BEI|UART_IE_PEI|UART_IE_FEI);
		
    /* Enable RX interrupt */
    SET_BIT(huart->Instance->IE,UART_IE_RXI); 
		
		return HAL_OK;
}

void BSP_UART_IRQHandler(UART_HandleTypeDef *huart)
{
	  /* receive complete */
		if(__HAL_UART_GET_FLAG(huart, UART_MIS_RXI))
    {
			/* clear the RXI flag */
		  WRITE_REG(huart->Instance->ICR, UART_ICR_RXI);
			
			/* Store 1 Data in buffer */
			huart->lu8_RxData[0] = huart->Instance->DR;
		
		  huart->lu8_RxBusy = false;
        
			/* Disable RX and RTI interrupt & clear error interrupt*/
			CLEAR_BIT(huart->Instance->IE, ( UART_IE_OEI | UART_IE_BEI | UART_IE_PEI | UART_IE_FEI)); //UART_IE_RXI |
			
			BSP_UART_RxCpltCallback(huart);
		}
		
		/* if UART error occurs */
		if(READ_REG(huart->Instance->RIS) & (UART_RIS_ERROR_BITS))
		{
			/* UART parity error interrupt occurred */
			if(__HAL_UART_GET_ERR_FLAG(huart, UART_RIS_PEI))
			{
        WRITE_REG(huart->Instance->ICR, UART_ICR_PEI);
        huart->ErrorCode |= HAL_UART_ERROR_PE; 
			}
			
			/* UART break error interrupt occurred */
			if(__HAL_UART_GET_ERR_FLAG(huart, UART_RIS_BEI))
			{
        WRITE_REG(huart->Instance->ICR, UART_ICR_BEI);
        huart->ErrorCode |= HAL_UART_ERROR_NE;
			}
			
			/* UART frame error interrupt occurred */
			if(__HAL_UART_GET_ERR_FLAG(huart, UART_RIS_FEI))
			{
        WRITE_REG(huart->Instance->ICR, UART_ICR_FEI);
        huart->ErrorCode |= HAL_UART_ERROR_FE;
			}
			
			/* UART Over-Run interrupt occurred */
			if(__HAL_UART_GET_ERR_FLAG(huart, UART_ICR_OEI))
			{
        WRITE_REG(huart->Instance->ICR, UART_ICR_OEI);
        huart->ErrorCode |= HAL_UART_ERROR_ORE;
			}

		  BSP_UART_ErrorCallback(huart); 
		}
}




/**** !!!THE FOLLOWING BSP FUNCTIONS END!!! ****/
//void UART2_IRQHandler(void)
//{
//	BSP_UART_IRQHandler(&UART_Handle);
//}

/* UART Interrupt handler */ 
void BSP_UART_RxCpltCallback(UART_HandleTypeDef *huart)
{	
	if(huart->Instance == UART2)
	{
    BSP_UART_Fram_Receive_IT_Handler(huart, &Uart_Fram_Struct, &rx_buf); //receive one byte

#if ENABLE_T35_TIMER_MODE
		__HAL_TIM_SET_COUNTER(&T35_TIM_Handler, 19);
		if(!__HAL_IS_TIM_ENABLE(&T35_TIM_Handler))
		{
			__HAL_TIM_ENABLE(&T35_TIM_Handler);
			HAL_TIM_ENABLE_IT(&T35_TIM_Handler, TIMER_INT_EN_UPD); 
		}
#endif
  }		
}

void BSP_UART_ErrorCallback(UART_HandleTypeDef *huart)
{

}
