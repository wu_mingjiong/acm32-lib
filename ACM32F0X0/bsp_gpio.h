/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __BSP_GPIO_H
#define __BSP_GPIO_H

/* Includes -------------------------------------------------------------------*/
#include "ACM32Fxx_HAL.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/
typedef struct {
    enum_GPIOx_t        gpio_port;
    GPIO_InitTypeDef    gpio_init_param;
}GPIO_InitStructTypedef;

/* Exported define ------------------------------------------------------------*/
// GPIO PIN Define
#define TEST_GPIO_PORT      GPIOA
#define TEST_GPIO_PIN       GPIO_PIN_11

#define LCD_RST_GPIO_Port   GPIOC       
#define LCD_RST_GPIO_Pin    GPIO_PIN_5  

#define LCD_BL_GPIO_Port    GPIOC
#define LCD_BL_GPIO_Pin     GPIO_PIN_4

// GPIO Operation 
#define TEST_GPIO_H()       HAL_GPIO_WritePin(TEST_GPIO_PORT, TEST_GPIO_PIN, GPIO_PIN_SET)
#define TEST_GPIO_L()       HAL_GPIO_WritePin(TEST_GPIO_PORT, TEST_GPIO_PIN, GPIO_PIN_CLEAR)

#define LCD_RST_H()         HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_GPIO_Pin, GPIO_PIN_SET)
#define LCD_RST_L()         HAL_GPIO_WritePin(LCD_RST_GPIO_Port, LCD_RST_GPIO_Pin, GPIO_PIN_CLEAR)

#define LCD_BL_H()          HAL_GPIO_WritePin(LCD_BL_GPIO_Port, LCD_BL_GPIO_Pin, GPIO_PIN_SET)
#define LCD_BL_L()          HAL_GPIO_WritePin(LCD_BL_GPIO_Port, LCD_BL_GPIO_Pin, GPIO_PIN_CLEAR)

/* Exported macro -------------------------------------------------------------*/
// 通用GPIO初始化组(AF0)
/* Port, Pin, Mode, Pull, Alternate */
#define GPIO_Init_Group_Data {                                                                                              \
                                {TEST_GPIO_PORT,    TEST_GPIO_PIN,    GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_FUNCTION_0},   \
                                {LCD_RST_GPIO_Port, LCD_RST_GPIO_Pin, GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_FUNCTION_0},   \
                                {LCD_BL_GPIO_Port,  LCD_BL_GPIO_Pin,  GPIO_MODE_OUTPUT_PP, GPIO_PULLUP, GPIO_FUNCTION_0},   \
                             }

// 通用GPIO初始化完成后设定初始状态(高/低电平)
#define GPIO_Init_State()   do{                 \
                                TEST_GPIO_H();  \
                                LCD_RST_H();    \
                                LCD_BL_H();     \
                            }while(0)

/* Exported variables ---------------------------------------------------------*/

/* Exported function prototypes -----------------------------------------------*/
void GPIO_Init(void);

#ifdef __cplusplus
}
#endif

#endif  /*__BSP_GPIO_H */
/******************************************* end of file **************************************************/
