/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Includes ------------------------------------------------------------------*/
#include "bsp_gpio.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/
static GPIO_InitStructTypedef _GPIO_Init_Group[] = GPIO_Init_Group_Data;

#define _GPIO_Init_Group_Size   sizeof(_GPIO_Init_Group)/sizeof(_GPIO_Init_Group[0])

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    GPIO_Init
  * @brief   普通IO初始化
  * @param   None
  * @retval  None
  * @author  Wumingjiong 
  * @Data    2023-11-09
 **/
/* -------------------------------- end -------------------------------- */
void GPIO_Init(void)
{
    GPIO_InitStructTypedef *gpio_data = _GPIO_Init_Group;
    
    for(uint16_t i = 0; i < _GPIO_Init_Group_Size; i++)
    {
        HAL_GPIO_Init(gpio_data[i].gpio_port, &gpio_data[i].gpio_init_param);
    }
    
    GPIO_Init_State();
}

/******************************************* end of file **************************************************/
