/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @FilePath: 
 */

/* Includes ------------------------------------------------------------------*/
#include "bsp_spi.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
SPI_HandleTypeDef spi1_handle, spi2_handle, spi3_handle;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* Application Layer ---------------------------------------------------------*/

/* Hardware Abstract Layer ---------------------------------------------------*/

/* Hardware Layer ------------------------------------------------------------*/
#if SPI1_ENABLE
void SPI1_Init(void)
{
	spi1_handle.Instance                 = SPI1;
	spi1_handle.Init.SPI_Mode            = SPI1_Mode;         
	spi1_handle.Init.SPI_Work_Mode       = SPI1_WORK_MODE;    
	spi1_handle.Init.X_Mode              = SPI1_X_Mode;      
	spi1_handle.Init.First_Bit           = SPI1_First_Bit;			
	spi1_handle.Init.BaudRate_Prescaler  = SPI1_Prescsaler;
	
	HAL_SPI_Init(&spi1_handle);

#if SPI1_CS_MODE == CS_PIN_SOFTWARE_MODE  /* 将CS管脚配置成普通GPIO */    
    
	GPIO_InitTypeDef gpio_cfg = {0};
    
	gpio_cfg.Pin       = SPI1_CS_PIN;
	gpio_cfg.Mode      = GPIO_MODE_OUTPUT_PP;
	gpio_cfg.Pull      = GPIO_PULLUP;
	gpio_cfg.Alternate = GPIO_FUNCTION_0;
	HAL_GPIO_Init(SPI1_CS_PORT, &gpio_cfg);
#endif
    MODIFY_REG(SPI1->CTL, SPI_CTL_X_MODE, SPI1_Init_X_MODE);
    HAL_GPIO_WritePin(SPI1_CS_PORT, SPI1_CS_PIN, SPI1_Init_CS_STA);
}
#endif

#if SPI2_ENABLE
void SPI2_Init(void)
{
	spi2_handle.Instance                 = SPI2;
	spi2_handle.Init.SPI_Mode            = SPI2_Mode;         
	spi2_handle.Init.SPI_Work_Mode       = SPI2_WORK_MODE;    
	spi2_handle.Init.X_Mode              = SPI2_X_Mode;      
	spi2_handle.Init.First_Bit           = SPI2_First_Bit;			
	spi2_handle.Init.BaudRate_Prescaler  = SPI2_Prescsaler;
	
	HAL_SPI_Init(&spi2_handle);
#if SPI2_CS_MODE == CS_PIN_SOFTWARE_MODE  /* 将CS管脚配置成普通GPIO */ 
	GPIO_InitTypeDef gpio_cfg = {0};
    
	gpio_cfg.Pin       = SPI2_CS_PIN;
	gpio_cfg.Mode      = GPIO_MODE_OUTPUT_PP;
	gpio_cfg.Pull      = GPIO_PULLUP;
	gpio_cfg.Alternate = GPIO_FUNCTION_0;
	HAL_GPIO_Init(SPI2_CS_PORT, &gpio_cfg);
#endif 
	MODIFY_REG(SPI2->CTL, SPI_CTL_X_MODE, SPI2_Init_X_MODE);
    HAL_GPIO_WritePin(SPI2_CS_PORT, SPI2_CS_PIN, SPI2_Init_CS_STA);
}
#endif

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    SPI_Init
  * @brief   SPI Total initialization
  * @param   None
  * @retval  None
 **/
/* -------------------------------- end -------------------------------- */
void SPI_Init(void)
{
#if SPI1_ENABLE
    SPI1_Init();
#endif
    
#if SPI2_ENABLE
    SPI2_Init();
#endif

}

#if SPI3_ENABLE
void spi3_xip_init(uint8_t Para1, uint16_t Para2)
{
	SPI3->CMD = (Para1 | (uint32_t)(Para2<<8));
	SPI3->PARA = 0;
	SPI3->MEMO_ACC = ((0x18<<14) | (1<<3) | (1<<1));
}

void spi3_xip_enable(void)
{
	GPIO_InitTypeDef    gpio_cfg = {0};

	/* Initialization GPIO CS pin */	
	gpio_cfg.Pin	   = SPI3_CS_PIN;
	gpio_cfg.Mode	   = GPIO_MODE_AF_PP;
	gpio_cfg.Pull	   = GPIO_PULLUP;
	gpio_cfg.Alternate = GPIO_FUNCTION_6;

	HAL_GPIO_Init(SPI3_CS_PORT, &gpio_cfg);
	SPI3_CS_H();

	SPI3->CTL = ((SPI3->CTL & (~(3<<5))) | SPI_1X_MODE);
	SPI3->MEMO_ACC |= SPI_ACC_EN;
}

void spi3_xip_disable(void)
{
	GPIO_InitTypeDef    gpio_cfg = {0};

	SPI3->MEMO_ACC &= (~SPI_ACC_EN);

	/* Initialization GPIO CS pin */
	gpio_cfg.Pin       = SPI3_CS_PIN;
	gpio_cfg.Mode      = GPIO_MODE_OUTPUT_PP;
	gpio_cfg.Pull      = GPIO_PULLUP;
	gpio_cfg.Alternate = GPIO_FUNCTION_0;

	HAL_GPIO_Init(SPI_PSRAM_CS_PORT, &gpio_cfg);
	
	SPI3_CS_H();
}
#endif

void HAL_SPI_MspInit(SPI_HandleTypeDef *hspi)
{
    /* 
      NOTE : This function should be modified by the user.
    */
    
    /* For Example */
    GPIO_InitTypeDef GPIO_Handle = {0}; 
    
    /* SPI1 */
    if (hspi->Instance == SPI1)
    {
        /* Enable Clock */
        System_Module_Enable(EN_SPI1);

        GPIO_Handle.Pin  = SPI1_CS_PIN;
        GPIO_Handle.Mode = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull = GPIO_PULLUP;
        GPIO_Handle.Alternate = SPI1_CS_PIN_AF;
        HAL_GPIO_Init(SPI1_CS_PORT, &GPIO_Handle);

        GPIO_Handle.Pin = SPI1_CLK_PIN;
        GPIO_Handle.Alternate = SPI1_CLK_PIN_AF;
        HAL_GPIO_Init(SPI1_CLK_PORT, &GPIO_Handle);

        GPIO_Handle.Pin = SPI1_MOSI_PIN;
        GPIO_Handle.Alternate = SPI1_MOSI_PIN_AF;
        HAL_GPIO_Init(SPI1_MOSI_PORT, &GPIO_Handle);
        
        GPIO_Handle.Pin = SPI1_MISO_PIN;
        GPIO_Handle.Alternate = SPI1_MISO_PIN_AF;
        HAL_GPIO_Init(SPI1_MISO_PORT, &GPIO_Handle);
        
        if (hspi->Init.X_Mode == SPI_4X_MODE) 
        {
            GPIO_Handle.Pin = SPI1_IO2_PIN;
            GPIO_Handle.Alternate = SPI1_IO2_PIN_AF;
            HAL_GPIO_Init(SPI1_IO2_PORT, &GPIO_Handle);
            
            GPIO_Handle.Pin = SPI1_IO3_PIN;
            GPIO_Handle.Alternate = SPI1_IO3_PIN_AF;
            HAL_GPIO_Init(SPI1_IO3_PORT, &GPIO_Handle);
        }
        
        /* Clear Pending Interrupt */
        NVIC_ClearPendingIRQ(SPI1_IRQn);
        
        /* Enable External Interrupt */
        NVIC_EnableIRQ(SPI1_IRQn);
    }
    /* SPI2 */
    else if (hspi->Instance == SPI2) 
    {
        /* Enable Clock */
        System_Module_Enable(EN_SPI2);
        
        GPIO_Handle.Pin  = SPI2_CS_PIN;
        GPIO_Handle.Mode = GPIO_MODE_AF_PP;
        GPIO_Handle.Pull = GPIO_PULLUP;
        GPIO_Handle.Alternate = SPI2_CS_PIN_AF;
        HAL_GPIO_Init(SPI2_CS_PORT, &GPIO_Handle);

        GPIO_Handle.Pin = SPI2_CLK_PIN;
        GPIO_Handle.Alternate = SPI2_CLK_PIN_AF;
        HAL_GPIO_Init(SPI2_CLK_PORT, &GPIO_Handle);

        GPIO_Handle.Pin = SPI2_MOSI_PIN;
        GPIO_Handle.Alternate = SPI2_MOSI_PIN_AF;
        HAL_GPIO_Init(SPI2_MOSI_PORT, &GPIO_Handle);
        
        GPIO_Handle.Pin = SPI2_MISO_PIN;
        GPIO_Handle.Alternate = SPI2_MISO_PIN_AF;
        HAL_GPIO_Init(SPI2_MISO_PORT, &GPIO_Handle);
        
        if (hspi->Init.X_Mode == SPI_4X_MODE) 
        {
            GPIO_Handle.Pin = SPI2_IO2_PIN;
            GPIO_Handle.Alternate = SPI2_IO2_PIN_AF;
            HAL_GPIO_Init(SPI2_IO2_PORT, &GPIO_Handle);
            
            GPIO_Handle.Pin = SPI2_IO3_PIN;
            GPIO_Handle.Alternate = SPI2_IO3_PIN_AF;
            HAL_GPIO_Init(SPI2_IO3_PORT, &GPIO_Handle);
        }
        
        /* Clear Pending Interrupt */
        NVIC_ClearPendingIRQ(SPI2_IRQn);
        
        /* Enable External Interrupt */
        NVIC_EnableIRQ(SPI2_IRQn);
    }
}

/* SPI Interrupt */
void SPI1_IRQHandler(void)
{
    HAL_SPI_IRQHandler(&spi1_handle);
}

void SPI2_IRQHandler(void)
{
    HAL_SPI_IRQHandler(&spi2_handle);	
}

