/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @FilePath: 
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __SPI_H
#define __SPI_H

/* Includes -------------------------------------------------------------------*/
#include "ACM32Fxx_HAL.h"

#ifdef __cplusplus
extern "C" {
#endif

/* User Defination ------------------------------------------------------------*/
#define SPI_INTF_FLASH                          SPI1
#define SPI_INTF_LCD                            SPI2

#define spi_lcd_handle                          spi2_handle
#define spi_flash_handle                        spi1_handle

#define SPI_FLASH_DMA_SEND_REQ_ID               SPI1_DMA_SEND_REQ_ID
#define SPI_FLASH_DMA_RCV_REQ_ID                SPI1_DMA_RCV_REQ_ID

#define SPI_LCD_DMA_SEND_REQ_ID                 SPI2_DMA_SEND_REQ_ID

#define SPI_FLASH_CS_PORT                       SPI1_CS_PORT
#define SPI_FLASH_CS_PIN                        SPI1_CS_PIN

#define SPI_LCD_CS_PORT                         SPI2_CS_PORT
#define SPI_LCD_CS_PIN                          SPI2_CS_PIN

#define SPI_FLASH_CS_H()                        SPI1_CS_H()
#define SPI_FLASH_CS_L()                        SPI1_CS_L()

#define SPI_LCD_CS_H()                          SPI2_CS_H()
#define SPI_LCD_CS_L()                          SPI2_CS_L()

/* Exported define ------------------------------------------------------------*/
#define SPI1_ENABLE 1
#define SPI2_ENABLE 1

#define CS_PIN_SOFTWARE_MODE  (1U)
#define CS_PIN_HARDWARE_MODE  (2U)

/* SPI Configuration */
#if SPI1_ENABLE 
    /* SPI1 Parameter Configuration */
    #define SPI1_Mode           SPI_MODE_MASTER
    #define SPI1_WORK_MODE      SPI_WORK_MODE_0
    #define SPI1_X_Mode         SPI_4X_MODE
    #define SPI1_First_Bit      SPI_FIRSTBIT_MSB
    #define SPI1_Prescsaler     SPI_BAUDRATE_PRESCALER_2
    
    /* SPI1 X_Mode初始状态， SPI初始化完成后设置为1/2/4线模式 */
    #define SPI1_Init_X_MODE    SPI_1X_MODE
    
    /* SPI1 CS引脚初始状态， SPI初始化完成后CS管脚设置为高/低电平 */
    #define SPI1_Init_CS_STA    GPIO_PIN_SET
    
    /* 选择使用软件/程序控制CS管脚,还是硬件SPI控制CS管脚 */
    #define SPI1_CS_MODE        CS_PIN_SOFTWARE_MODE
    
    /* SPI1 CS Pin */
    #define SPI1_CS_PORT        GPIOA
    #define SPI1_CS_PIN         GPIO_PIN_4
    #define SPI1_CS_PIN_AF      GPIO_FUNCTION_1
    
    /* SPI1 CLK Pin */
    #define SPI1_CLK_PORT       GPIOA
    #define SPI1_CLK_PIN        GPIO_PIN_5
    #define SPI1_CLK_PIN_AF     GPIO_FUNCTION_1
    
    /* SPI1 MOSI Pin */
    #define SPI1_MOSI_PORT      GPIOB
    #define SPI1_MOSI_PIN       GPIO_PIN_5
    #define SPI1_MOSI_PIN_AF    GPIO_FUNCTION_1
    
    /* SPI1 MISO Pin */
    #define SPI1_MISO_PORT      GPIOB
    #define SPI1_MISO_PIN       GPIO_PIN_4
    #define SPI1_MISO_PIN_AF    GPIO_FUNCTION_1
    
    /* SPI1 IO2 Pin */
    #define SPI1_IO2_PORT       GPIOA
    #define SPI1_IO2_PIN        GPIO_PIN_7
    #define SPI1_IO2_PIN_AF     GPIO_FUNCTION_4
    
    /* SPI1 IO3 Pin */
    #define SPI1_IO3_PORT       GPIOA
    #define SPI1_IO3_PIN        GPIO_PIN_6
    #define SPI1_IO3_PIN_AF     GPIO_FUNCTION_4
#endif

#if SPI2_ENABLE 
    /* SPI2 Parameter Configuration */
    #define SPI2_Mode           SPI_MODE_MASTER
    #define SPI2_WORK_MODE      SPI_WORK_MODE_3
    #define SPI2_X_Mode         SPI_4X_MODE
    #define SPI2_First_Bit      SPI_FIRSTBIT_MSB
    #define SPI2_Prescsaler     SPI_BAUDRATE_PRESCALER_2
    
    /* SPI2 X_Mode初始状态， SPI初始化完成后设置为1/2/4线模式 */
    #define SPI2_Init_X_MODE    SPI_4X_MODE
    
    /* SPI2 CS引脚初始状态， SPI初始化完成后CS管脚设置为高/低电平 */
    #define SPI2_Init_CS_STA    GPIO_PIN_SET
    
    /* 选择使用软件/程序控制CS管脚,还是硬件SPI控制CS管脚 */
    #define SPI2_CS_MODE        CS_PIN_SOFTWARE_MODE
    
    /* SPI2 CS Pin */
    #define SPI2_CS_PORT        GPIOB
    #define SPI2_CS_PIN         GPIO_PIN_9
    #define SPI2_CS_PIN_AF      GPIO_FUNCTION_4
    
    /* SPI2 CLK Pin */
    #define SPI2_CLK_PORT       GPIOB
    #define SPI2_CLK_PIN        GPIO_PIN_8
    #define SPI2_CLK_PIN_AF     GPIO_FUNCTION_4
    
    /* SPI2 MOSI Pin */
    #define SPI2_MOSI_PORT      GPIOB
    #define SPI2_MOSI_PIN       GPIO_PIN_7
    #define SPI2_MOSI_PIN_AF    GPIO_FUNCTION_4
    
    /* SPI2 MISO Pin */
    #define SPI2_MISO_PORT      GPIOB
    #define SPI2_MISO_PIN       GPIO_PIN_6
    #define SPI2_MISO_PIN_AF    GPIO_FUNCTION_4
    
    /* SPI2 IO2 Pin */
    #define SPI2_IO2_PORT       GPIOC
    #define SPI2_IO2_PIN        GPIO_PIN_7
    #define SPI2_IO2_PIN_AF     GPIO_FUNCTION_2
    
    /* SPI2 IO3 Pin */
    #define SPI2_IO3_PORT       GPIOC
    #define SPI2_IO3_PIN        GPIO_PIN_6
    #define SPI2_IO3_PIN_AF     GPIO_FUNCTION_2
#endif

/* Exported macro -------------------------------------------------------------*/
//F4XX SPI3 Mapping Addr
#define XIP_MEM_BASE                        0x90000000

/* Exported macro -------------------------------------------------------------*/

/**************************** Software controls CS pins **************************************/
#if SPI1_CS_MODE == CS_PIN_SOFTWARE_MODE
    #define SPI1_CS_H()     HAL_GPIO_WritePin(SPI1_CS_PORT, SPI1_CS_PIN, GPIO_PIN_SET)
    #define SPI1_CS_L()     HAL_GPIO_WritePin(SPI1_CS_PORT, SPI1_CS_PIN, GPIO_PIN_CLEAR)
#else
    #define SPI1_CS_H()
    #define SPI1_CS_L()
#endif

#if SPI2_CS_MODE == CS_PIN_SOFTWARE_MODE
    #define SPI2_CS_H()     HAL_GPIO_WritePin(SPI2_CS_PORT, SPI2_CS_PIN, GPIO_PIN_SET)
    #define SPI2_CS_L()     HAL_GPIO_WritePin(SPI2_CS_PORT, SPI2_CS_PIN, GPIO_PIN_CLEAR)
#else
    #define SPI2_CS_H()                 
    #define SPI2_CS_L()                 
#endif

#if SPI3_CS_MODE == CS_PIN_SOFTWARE_MODE
    #define SPI3_CS_H()     HAL_GPIO_WritePin(SPI3_CS_PORT, SPI3_CS_PIN, GPIO_PIN_SET)
    #define SPI3_CS_L()     HAL_GPIO_WritePin(SPI3_CS_PORT, SPI3_CS_PIN, GPIO_PIN_CLEAR)
#else
    #define SPI3_CS_H()
    #define SPI3_CS_L()
#endif

/******************** SPI DMA REQ**********************/
#define SPI1_DMA_SEND_REQ_ID        REQ1_SPI1_SEND
#define SPI1_DMA_RCV_REQ_ID         REQ2_SPI1_RECV

#define SPI2_DMA_SEND_REQ_ID        REQ3_SPI2_SEND
#define SPI2_DMA_RCV_REQ_ID         REQ4_SPI2_RECV

#define SPI3_DMA_SEND_REQ_ID        REQ47_SPI3_SEND
#define SPI3_DMA_RCV_REQ_ID         REQ48_SPI3_RECV

/* SPI1 X_MODE Config */
#define SPI1_SET_1X_MODE()          MODIFY_REG(SPI1->CTL, SPI_CTL_X_MODE, SPI_1X_MODE)
#define SPI1_SET_2X_MODE()          MODIFY_REG(SPI1->CTL, SPI_CTL_X_MODE, SPI_2X_MODE)
#define SPI1_SET_4X_MODE()          MODIFY_REG(SPI1->CTL, SPI_CTL_X_MODE, SPI_4X_MODE)

/* SPI2 X_MODE Config */
#define SPI2_SET_1X_MODE()          MODIFY_REG(SPI2->CTL, SPI_CTL_X_MODE, SPI_1X_MODE)
#define SPI2_SET_2X_MODE()          MODIFY_REG(SPI2->CTL, SPI_CTL_X_MODE, SPI_2X_MODE)
#define SPI2_SET_4X_MODE()          MODIFY_REG(SPI2->CTL, SPI_CTL_X_MODE, SPI_4X_MODE)

/* User Defination */
#define psram_xip_init 	            spi3_xip_init(CSS1604L_CMD_RD, CSS1604L_CMD_WR)

/* Exported variables ---------------------------------------------------------*/
extern SPI_HandleTypeDef spi1_handle, spi2_handle, spi3_handle;

/* Exported function prototypes -----------------------------------------------*/
#if SPI1_ENABLE 
void SPI1_Init(void);
#endif
#if SPI2_ENABLE 
void SPI2_Init(void);
#endif

#if SPI3_ENABLE
void spi3_xip_init(uint8_t Para1, uint16_t Para2);
void spi3_xip_enable(void);
void spi3_xip_disable(void);
#endif

// ALL SPI Initialization
void SPI_Init(void);

#ifdef __cplusplus
}
#endif

#endif  /*__SPI_H */
/******************************************* end of file **************************************************/
