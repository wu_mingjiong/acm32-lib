/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __BSP_DMA_H
#define __BSP_DMA_H

/* Includes -------------------------------------------------------------------*/
#include "ACM32Fxx_HAL.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/

/* User Defination ------------------------------------------------------------*/

/* Exported macro -------------------------------------------------------------*/
#define DMA_TRANS_MAX                   (256*8)
#define DMA_DATA_FLOW_P2P               (0x00001800)

#define DMA_CHANNEL_CTRL_SBSIZE         (BIT12|BIT13|BIT14)
#define DMA_CHANNEL_CTRL_SBSIZE_POS     (12)

#define DMA_CHANNEL_CTRL_DBSIZE         (BIT15|BIT16|BIT17)
#define DMA_CHANNEL_CTRL_DBSIZE_POS     (15)


/* Exported variables ---------------------------------------------------------*/

/* Exported function prototypes -----------------------------------------------*/
#define __HAL_DMA_BURST_SBSIZE_CONFIG(__HANDLE__, __SIZE__)  MODIFY_REG((__HANDLE__)->CTRL, DMA_CHANNEL_CTRL_SBSIZE, (__SIZE__) << DMA_CHANNEL_CTRL_SBSIZE_POS)
#define __HAL_DMA_BURST_DBSIZE_CONFIG(__HANDLE__, __SIZE__)  MODIFY_REG((__HANDLE__)->CTRL, DMA_CHANNEL_CTRL_DBSIZE, (__SIZE__) << DMA_CHANNEL_CTRL_DBSIZE_POS)

#ifdef __cplusplus
}
#endif

#endif  /*__BSP_DMA_H */
/******************************************* end of file **************************************************/
