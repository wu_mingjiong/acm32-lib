/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __SEGGER_RTT_USER_H
#define __SEGGER_RTT_USER_H

/* Includes -------------------------------------------------------------------*/
#include "SEGGER_RTT.h"
#include "SEGGER_RTT_Conf.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/

/* Exported define ------------------------------------------------------------*/

/* Exported macro -------------------------------------------------------------*/

/* Exported variables ---------------------------------------------------------*/

/* Exported function prototypes -----------------------------------------------*/
void segger_rtt_init(char * str);
void print_log(const char * sFormat, ...);

#ifdef __cplusplus
}
#endif

#endif  /*__SEGGER_RTT_USER_H */
/******************************************* end of file **************************************************/
