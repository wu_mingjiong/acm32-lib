#include "compiler_define.h"

#if (!__IS_USE_SEMIHOSTING__)
    #if __IS_COMPILER_ARM_COMPILER_6__
        __asm(".global __use_no_semihosting");
    #elif __IS_COMPILER_ARM_COMPILER_5__
        #pragma import(__use_no_semihosting)
    #endif

    #if __IS_COMPILER_ARM_COMPILER_6__
        void _sys_exit(int ret)
        {
            (void)ret;
            while(1) {}
        }
    #endif

    #if __IS_COMPILER_ARM_COMPILER__
        void _ttywrch(int ch)
        {
            ARM_2D_UNUSED(ch);
        }
    #endif
#endif