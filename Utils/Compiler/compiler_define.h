#ifndef __COMPILER_DEFINE_H
#define __COMPILER_DEFINE_H

/**< AC5 Compiler */
#undef __IS_COMPILER_ARM_COMPILER_5__
#if defined ( __CC_ARM )
    #define __IS_COMPILER_ARM_COMPILER_5__  1
#endif

/**< AC6 Compiler */
#undef __IS_COMPILER_ARM_COMPILER_6__
#if defined(__ARMCC_VERSION) && (__ARMCC_VERSION >= 6010050)
    #define __IS_COMPILER_ARM_COMPILER_6__  1
#endif

/**< IAR Compiler */
#undef __IS_COMPILER_IAR__
#if defined ( __ICCARM__ )   
    #define __IS_COMPILER_IAR__             1
#endif

#undef  __IS_COMPILER_LLVM__
#undef __IS_COMPILER_GCC__
#if defined(__clang__) && !__IS_COMPILER_ARM_COMPILER_6__   /**< LLVM Compiler */
    #define __IS_COMPILER_LLVM__            1
#else
#   if defined(__GNUC__) && !(  defined(__IS_COMPILER_ARM_COMPILER__)   \
                            ||  defined(__IS_COMPILER_LLVM__)           \
                            ||  defined(__IS_COMPILER_IAR__))
        #define __IS_COMPILER_GCC__         1               /**< GCC Compiler */
    #endif
#endif

/**< main function argv */
#if __IS_COMPILER_ARM_COMPILER_6__
    #ifndef __MICROLIB  /**< Microlib */
        __asm(".global __ARM_use_no_argv\n\t");
    #endif
#endif

/**< Setting */
#ifndef __IS_USE_SEMIHOSTING__  /* choose whether use semi-hosting mode */
    #define __IS_USE_SEMIHOSTING__          1
#endif

#endif