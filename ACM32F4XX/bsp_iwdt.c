/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Includes ------------------------------------------------------------------*/
#include "bsp_iwdt.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/

/* Private variables ---------------------------------------------------------*/
IWDT_HandleTypeDef hiwdt;

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/******************************************* end of file **************************************************/
void IWDT_Init(void)
{
    hiwdt.Instance = IWDT;
    hiwdt.Init.Prescaler = IWDT_PRESCALER;  
    hiwdt.Init.Reload    = IWDT_RELOAD_VAL;    
    hiwdt.Init.Window    = IWDT_WINDOW_VAL;  
    hiwdt.Init.Wakeup    = IWDT_WAKEUP_VAL;   
    
    HAL_IWDT_Init(&hiwdt);    
}