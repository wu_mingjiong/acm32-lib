/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Includes ------------------------------------------------------------------*/
#include "bsp_adc.h"

/* Private typedef -----------------------------------------------------------*/

/* Private define ------------------------------------------------------------*/

/* Private macro -------------------------------------------------------------*/
#define _ADC_CHANNELS_LIST_NUM  (sizeof(ADC_Channels_Config_List)/sizeof(ADC_Channels_Config_List[0]))

/* Private variables ---------------------------------------------------------*/
ADC_HandleTypeDef ADC_Handle;

volatile uint32_t VrefP;

static ADC_ChannelConfTypeDef ADC_Channels_Config_List[] = ADC_CHANNELS_CONFIG; 

/* Private function prototypes -----------------------------------------------*/

/* Private functions ---------------------------------------------------------*/

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    BSP_ADC_Channel_Init
  * @brief   板级ADC通道初始化
  * @param   adc_handle: [输入/出] 
**			 adc_ch_conf_list: [输入/出] 
**			 list_size: [输入/出] 
  * @retval
  * @author  Wumingjiong 
  * @Data    2023-10-25
 **/
/* -------------------------------- end -------------------------------- */
static void BSP_ADC_Channel_Init(ADC_HandleTypeDef *adc_handle, ADC_ChannelConfTypeDef *adc_ch_conf_list,uint8_t list_size)
{
    uint8_t i;
    
    adc_handle->ChannelNum = list_size;
    
    for(i = 0; i < list_size; i++)
    {
        HAL_ADC_ConfigChannel(adc_handle, &adc_ch_conf_list[i]);
    }
}

/* -------------------------------- begin  -------------------------------- */
/**
  * @Name    ADC_GetVrefP
  * @brief   利用BGR通道校准参考电压，采集BGR通道(3V)，并通过BGR得出的电压换算成参考电压
  * @param   Pre_Channel: 预采样通道(通道2/8/10其中任一通道)
  * @retval  VrefP, 参考电压，接近3.3V
  * @author   
  * @Data    2023-10-24
 **/
/* -------------------------------- end -------------------------------- */
uint32_t ADC_GetVrefP(uint8_t Pre_Channel)
{
    ADC_ChannelConfTypeDef ADC_ChannelConf;
    uint32_t TrimValue_3v, AdcValue_VrefP[2], VrefP,temp;
    
    ADC_Handle.Init.ClockDiv = ADC_CLOCK_DIV8;
    ADC_Handle.Init.ConConvMode = ADC_CONCONVMODE_DISABLE;
    ADC_Handle.Init.JChannelMode = ADC_JCHANNELMODE_DISABLE;
    ADC_Handle.Init.DiffMode = ADC_DIFFMODE_DISABLE;
    ADC_Handle.Init.DMAMode = ADC_DMAMODE_DISABLE;
    ADC_Handle.Init.OverMode = ADC_OVERMODE_DISABLE;
    ADC_Handle.Init.OverSampMode = ADC_OVERSAMPMODE_DISABLE;
    ADC_Handle.Init.AnalogWDGEn = ADC_ANALOGWDGEN_DISABLE;
    ADC_Handle.Init.ExTrigMode.ExTrigSel = ADC_SOFTWARE_START;
    ADC_Handle.Init.ChannelEn = ADC_CHANNEL_VBGR_EN;

    ADC_Handle.Instance = ADC;
        
    HAL_ADC_Init(&ADC_Handle);

    /* The total adc regular channels number */
    ADC_Handle.ChannelNum = 2;
    
     /* Add adc channels */
    ADC_ChannelConf.Channel = Pre_Channel;
    ADC_ChannelConf.RjMode = 0;
    ADC_ChannelConf.Sq = ADC_SEQUENCE_SQ1;
    ADC_ChannelConf.Smp = ADC_SMP_CLOCK_320;    
    HAL_ADC_ConfigChannel(&ADC_Handle,&ADC_ChannelConf);
    
    /* Add adc channels */
    ADC_ChannelConf.Channel = ADC_CHANNEL_VBGR;
    ADC_ChannelConf.RjMode = 0;
    ADC_ChannelConf.Sq = ADC_SEQUENCE_SQ2;
    ADC_ChannelConf.Smp = ADC_SMP_CLOCK_320;    
    HAL_ADC_ConfigChannel(&ADC_Handle,&ADC_ChannelConf);
    
    HAL_ADC_Polling(&ADC_Handle, AdcValue_VrefP, ADC_Handle.ChannelNum, 0);
    
    printfS("The adc convert result :  0x%08x \r\n", AdcValue_VrefP[1]);
    
    TrimValue_3v = *(volatile uint32_t*)(0x00080240); //Read the 1.2v trim value in 3.0v vrefp.

    printfS("The adc 1.2v trim value is : 0x%08x \r\n", TrimValue_3v);

    if(((~TrimValue_3v&0xFFFF0000)>>16) == (TrimValue_3v&0x0000FFFF))
    {
        temp = TrimValue_3v & 0xFFF;
        
        VrefP = (uint32_t)(temp * 3000 / (AdcValue_VrefP[1] & 0xFFF));
                
        return VrefP;
    }
    else
        return 0;
}

void ADC_Init(void)
{   
    ADC_Handle.Init.ClockDiv = ADC_CLOCK_DIV8;
    ADC_Handle.Init.ConConvMode = ADC_CONCONVMODE_DISABLE;
    ADC_Handle.Init.JChannelMode = ADC_JCHANNELMODE_DISABLE;
    ADC_Handle.Init.DiffMode = ADC_DIFFMODE_DISABLE;
    ADC_Handle.Init.DMAMode = ADC_DMAMODE_DISABLE;
    ADC_Handle.Init.OverMode = ADC_OVERMODE_DISABLE;
    ADC_Handle.Init.OverSampMode = ADC_OVERSAMPMODE_DISABLE;
    ADC_Handle.Init.AnalogWDGEn = ADC_ANALOGWDGEN_DISABLE;
    ADC_Handle.Init.ExTrigMode.ExTrigSel = ADC_SOFTWARE_START;
    ADC_Handle.Init.ChannelEn = ADC_CHANNELS_EN_FLAG;

    ADC_Handle.Instance = ADC;
        
    HAL_ADC_Init(&ADC_Handle);

    /* The total adc regular channels number */
    BSP_ADC_Channel_Init(&ADC_Handle, ADC_Channels_Config_List, _ADC_CHANNELS_LIST_NUM);
    
}

/* Usage Example */
uint32_t gu32_AdcBuffer[_ADC_CHANNELS_LIST_NUM];
void BSP_ADC_Test(void)
{
    uint32_t i, Voltage;
    
    uint32_t lu32_COM_OK  = 0;

    VrefP = ADC_GetVrefP(ADC_CHANNEL_8);
    
    printfS("The VrefP value is : %d \r\n", VrefP);

    ADC_Init();

    while(1)
    {
        for (i = 0; i < _ADC_CHANNELS_LIST_NUM; i++)
        {
            gu32_AdcBuffer[i] = 0;
        }
        
        HAL_ADC_Polling(&ADC_Handle, gu32_AdcBuffer, ADC_Handle.ChannelNum, 0);
        
        for (i = 0; i < ADC_Handle.ChannelNum; i++)
        {
            printfS("The adc convert result : Channel %d = 0x%08x. ", gu32_AdcBuffer[i]>>16 & 0xFF, gu32_AdcBuffer[i]);
            Voltage = (gu32_AdcBuffer[i]&0xFFF)*VrefP/4095;
            printfS("The Voltage is: %d mV \r\n", Voltage);
            
            lu32_COM_OK++;
        }
        
        printfS("ADC Test OK count %d times \r\n",  lu32_COM_OK);
        
        System_Delay_MS(2000);
 
    }
}

/******************************************* end of file **************************************************/
