/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __EFLASH_EX_H
#define __EFLASH_EX_H

/* Includes -------------------------------------------------------------------*/
#include "f4_f3.h"
#include "fxx_std.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/

/* Exported define ------------------------------------------------------------*/
#define REG(x)          (*(volatile UINT32 *)(x))  
#define PAGE_SIZE		512U 

/* Exported macro -------------------------------------------------------------*/
#define EFlash_ErasePage(addr)              EFlash_ErasePage(addr)
#define EFlash_Program_Word(addr, data)     EFlash_Program_Word(addr, data)

/* Exported variables ---------------------------------------------------------*/

/* Exported function prototypes -----------------------------------------------*/
void EFlash_Remap_Enable(void);
void EFlash_JTAG_Enable(void);
void EFlash_JTAG_Disable(void);
void EFlash_Option_LOCK(void);
    
#ifdef __cplusplus
}
#endif

#endif  /*__EFLASH_EX_H */
/******************************************* end of file **************************************************/
