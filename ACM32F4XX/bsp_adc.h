/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __BSP_ADC_H
#define __BSP_ADC_H

/* Includes -------------------------------------------------------------------*/
#include "ACM32Fxx_HAL.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/

/* Exported define ------------------------------------------------------------*/

/* Exported macro -------------------------------------------------------------*/
// ADC通道使能位，添加需要使能的通道
#define ADC_CHANNELS_EN_FLAG    (ADC_CHANNEL_0_EN | \
                                 ADC_CHANNEL_1_EN | \
                                 ADC_CHANNEL_2_EN | \
                                 ADC_CHANNEL_4_EN | \
                                 ADC_CHANNEL_5_EN)

// ADC通道参数配置,每个元素成员顺序为{RjMode, Channel, Sq, Smp}
#define ADC_CHANNELS_CONFIG    {                                                                \
                                    {0, ADC_CHANNEL_0, ADC_SEQUENCE_SQ1, ADC_SMP_CLOCK_320},    \
                                    {0, ADC_CHANNEL_1, ADC_SEQUENCE_SQ2, ADC_SMP_CLOCK_320},    \
                                    {0, ADC_CHANNEL_2, ADC_SEQUENCE_SQ3, ADC_SMP_CLOCK_320},    \
                                    {0, ADC_CHANNEL_4, ADC_SEQUENCE_SQ4, ADC_SMP_CLOCK_320},    \
                                    {0, ADC_CHANNEL_5, ADC_SEQUENCE_SQ5, ADC_SMP_CLOCK_320},    \
                                }

/* Exported variables ---------------------------------------------------------*/
extern ADC_HandleTypeDef ADC_Handle;
                                
/* Exported function prototypes -----------------------------------------------*/
uint32_t ADC_GetVrefP(uint8_t Pre_Channel);
void ADC_Init(void);                  
                                
void BSP_ADC_Test(void);

#ifdef __cplusplus
}
#endif

#endif  /*__BSP_ADC_H */
/******************************************* end of file **************************************************/
