#include "bsp_eflash_ex.h"

static void EFlash_ReWrite_Word(UINT32 addr, UINT32 value)	
{
	UINT32 buff[128];
	UINT32 i;
	UINT32 *dst;
	UINT32 dst_addr;
	UINT32 page_addr;  		

	if(REG(addr)==value)
	{
		return;
	}
	
	page_addr = addr&0xFFFFFE00;
	
	dst = (UINT32 *)(page_addr);
    
	for(i=0;i<(PAGE_SIZE/4); i++)
	{
		buff[i]=*dst++;
	}
	
    buff[(addr-page_addr)/4] = value; 
	
	EFlash_ErasePage(page_addr);
	
	dst_addr = page_addr;
	
    for(i=0;i<(PAGE_SIZE/4); i++)
	{
		EFlash_Program_Word(dst_addr,buff[i]);
		dst_addr +=4;
	}		
}

void EFlash_Remap_Enable(void)	       
{
    EFlash_ReWrite_Word(0x00080400, 0x89BC3F51U);      
}

void EFlash_JTAG_Enable(void)	 
{
    EFlash_ReWrite_Word(0x0008041C, 0xFFFFFFFFU);      
}  

void EFlash_JTAG_Disable(void)	 
{
    EFlash_ReWrite_Word(0x0008041C, 0x89BC3F51U);        
}

void EFlash_Option_LOCK(void) 	 
{
    EFlash_ReWrite_Word(0x000805FC, 0x55AA77EEU);        
}