/*
 * @Author: Wu
 * @Date: 
 * @LastEditTime:
 * @LastEditors: Wu
 * @Description: 
 * @Encoding:
 */

/* Define to prevent recursive inclusion --------------------------------------*/
#ifndef __BSP_IWDT_H
#define __BSP_IWDT_H

/* Includes -------------------------------------------------------------------*/
#include  "ACM32Fxx_HAL.h"

#ifdef __cplusplus
extern "C" {
#endif

/* Exported typedef -----------------------------------------------------------*/

/* Exported define ------------------------------------------------------------*/
#define IWDT_PRESCALER  IWDT_CLOCK_PRESCALER_32

#define IWDT_RELOAD_VAL IWDT_RELOAD_MAX_VALUE

// window function disabled when window >= reload 
#define IWDT_WINDOW_VAL IWDT_RELOAD_MAX_VALUE

// wakeup function disabled when wakeup >= reload  
#define IWDT_WAKEUP_VAL IWDT_RELOAD_MAX_VALUE

#define IWDT_FEED_DOG() HAL_IWDT_Kick_Watchdog_Wait_For_Done(&hiwdt)

/* Exported macro -------------------------------------------------------------*/

/* Exported variables ---------------------------------------------------------*/
extern IWDT_HandleTypeDef hiwdt;

/* Exported function prototypes -----------------------------------------------*/
void IWDT_Init(void);

#ifdef __cplusplus
}
#endif

#endif  /*__BSP_IWDT_H */
/******************************************* end of file **************************************************/

extern IWDT_HandleTypeDef hiwdt; 